package com.aeolus.forecast;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.aeolus.forecast.model.DataManager;
import com.aeolus.forecast.presenter.Presenter;
import com.aeolus.forecast.view.HolderActivity;

/**
 * Created by Yuriy on 13.01.2018.
 */

public class StartingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataManager.setPresenter(Presenter.getInstance());
        Presenter.getInstance().setDataManager(DataManager.getInstance());
        HolderActivity.setPresenter(Presenter.getInstance());
        startActivity(new Intent(this, HolderActivity.class));
        this.finish();
    }
}

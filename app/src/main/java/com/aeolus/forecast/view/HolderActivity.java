package com.aeolus.forecast.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.aeolus.forecast.presenter.IPresenter;
import com.aeolus.forecast.R;
import com.aeolus.forecast.view.vo.CityVO;
import com.aeolus.forecast.view.vo.DailyWeatherVO;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HolderActivity extends AppCompatActivity implements IView, OnFragmentReadyListener {

    private static IPresenter presenter;

    private Fragment currentFragment;

    public static void setPresenter(IPresenter presenter) {
        HolderActivity.presenter = presenter;
    }

    @BindView(R.id.activity_holder_root_view)
    RelativeLayout rootView;

    @BindView(R.id.curtain)
    View curtain;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holder);
        ButterKnife.bind(this);
        presenter.setView(this);
        if (getSupportFragmentManager().findFragmentById(R.id.activity_holder_fragment_container) == null)
            presenter.onInit();
        else
            currentFragment = getSupportFragmentManager().findFragmentById(R.id.activity_holder_fragment_container);
     //   new Timer().schedule(new FadeOutTask(), 1000);
    }

    @Override
    public void openWeatherPage() {
        if (getSupportFragmentManager().findFragmentById(R.id.activity_holder_fragment_container) == null) {
            currentFragment = new WeatherFragment();
            ((WeatherFragment) currentFragment).setPresenter(presenter);
            setFragment(currentFragment);
        } else {
            currentFragment = getSupportFragmentManager().findFragmentById(R.id.activity_holder_fragment_container);
            if (!(currentFragment instanceof WeatherFragment)) {
                currentFragment = new WeatherFragment();
                ((WeatherFragment) currentFragment).setPresenter(presenter);
                replaceFragment(currentFragment);
            }
        }
    }

    @Override
    public void openLocationPage() {
        if (getSupportFragmentManager().findFragmentById(R.id.activity_holder_fragment_container) == null) {
            currentFragment = new LocationsListFragment();
            ((LocationsListFragment) currentFragment).setPresenter(presenter);
            setFragment(currentFragment);
        } else {
            currentFragment = getSupportFragmentManager().findFragmentById(R.id.activity_holder_fragment_container);
            if (!(currentFragment instanceof LocationsListFragment)) {
                currentFragment = new LocationsListFragment();
                ((LocationsListFragment) currentFragment).setPresenter(presenter);
                replaceFragment(currentFragment);
            }
        }
    }

    @Override
    public void onNestedFragmentReady() {
        presenter.onViewCreated();
    }

    @Override
    public void showWeather(DailyWeatherVO weather) {
        ((WeatherFragment) currentFragment).showWeather(weather);
    }

    @Override
    public void appendCity(CityVO city) {
        try {
            ((LocationsListFragment) currentFragment).appendCity(city);
        }catch (Exception e) {
        }
    }

    @Override
    public void showExitNotice() {
        showToast(getString(R.string.exit_message));
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    @Override
    public void closeApp() {
        this.finish();
    }

    @Override
    public Context getContext() {
        return this;
    }

    void setFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.activity_holder_fragment_container, fragment)
                .commit();
    }

    void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_holder_fragment_container, fragment)
                .addToBackStack("tag")
                .commit();
    }

    public void updateModOn(){
        progressBar.setVisibility(View.VISIBLE);
        try {
            ((WeatherFragment) currentFragment).lockScreen(true);
        } catch (ClassCastException e){

        }
    }

    public void updateModOff(){
        progressBar.setVisibility(View.GONE);
        try {
            ((WeatherFragment) currentFragment).lockScreen(false);
        } catch (ClassCastException e){

        }
    }
}

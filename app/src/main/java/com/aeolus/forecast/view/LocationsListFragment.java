package com.aeolus.forecast.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aeolus.forecast.R;
import com.aeolus.forecast.presenter.IPresenter;
import com.aeolus.forecast.view.vo.CityVO;
import com.transitionseverywhere.Rotate;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 15.01.2018.
 */

public class LocationsListFragment extends Fragment {

    public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityHolder> {

        public class CityHolder extends RecyclerView.ViewHolder {
            ImageView weatherIcon;
            TextView cityNameTV;
            TextView temperatureTV;

            CityHolder(View view) {
                super(view);
                weatherIcon = view.findViewById(R.id.list_city_card_weather_icon_iv);
                cityNameTV = view.findViewById(R.id.list_city_card_city_name_tv);
                temperatureTV = view.findViewById(R.id.list_city_card_temperature_tv);
            }
        }

        List<CityVO> cities;

        CityAdapter(List<CityVO> cities) {
            this.cities = cities;
        }

        @Override
        public int getItemCount() {
            return cities.size();
        }

        @Override
        public CityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_city_card, parent, false);
            return new CityHolder(v);
        }

        @Override
        public void onBindViewHolder(CityHolder holder, int position) {
            CityVO city = cities.get(position);
            switch (city.getCurrentWeather()) {
                case FAIR:
                    holder.weatherIcon.setImageResource(R.drawable.ic_weather_sun);
                    break;
                case CLOUDY:
                    holder.weatherIcon.setImageResource(R.drawable.ic_weather_cloud);
                    break;
                case PRECIPITATION:
                    holder.weatherIcon.setImageResource(R.drawable.ic_weather_rain);
                    break;
            }
            holder.cityNameTV.setText(city.getCityName());
            holder.temperatureTV.setText("" + city.getTemperature());
        }
    }

    private OnFragmentReadyListener listener;

    private static IPresenter presenter;

    private CityAdapter adapter;

    @BindView(R.id.fragment_locations_toolbar)
    Toolbar toolbar;

    @BindView(R.id.fragment_locations_toolbar_update_icon)
    ImageView updateIcon;

    @BindView(R.id.fragment_locations_list_recycler_view)
    RecyclerView recyclerView;

    public void setPresenter(IPresenter presenter) {
        LocationsListFragment.presenter = presenter;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.listener = (OnFragmentReadyListener) context;
        } catch (final ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnCompleteListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_locations_list, container, false);
        ButterKnife.bind(this, view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CityAdapter(new ArrayList<>());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                presenter.onCityChosen(adapter.cities.get(position).getCityName());
            }

            @Override
            public void onLongItemClick(View view, int position) {
            }
        }));
        toolbar.setTitle("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        updateIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Transition rotate = new Rotate();
                rotate.setDuration(500);
                rotate.addListener(new Transition.TransitionListener() {
                    @Override
                    public void onTransitionStart(Transition transition) {

                    }

                    @Override
                    public void onTransitionEnd(Transition transition) {
                        updateIcon.setRotation(0);
                    }

                    @Override
                    public void onTransitionCancel(Transition transition) {
                        updateIcon.setRotation(0);
                    }

                    @Override
                    public void onTransitionPause(Transition transition) {
                        updateIcon.setRotation(0);
                    }

                    @Override
                    public void onTransitionResume(Transition transition) {

                    }
                });
                TransitionManager.beginDelayedTransition(toolbar, rotate);
                updateIcon.setRotation(360);
                presenter.onUpdatePressed();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.onNestedFragmentReady();
    }

    @Override
    public void onDestroyView() {
        adapter.cities.clear();
        adapter.notifyDataSetChanged();
        super.onDestroyView();
    }

    public synchronized void appendCity(CityVO city) {
        if (!adapter.cities.contains(city)) {
            adapter.cities.add(city);
            adapter.notifyItemInserted(adapter.cities.size() - 1);
        }
    }
}

package com.aeolus.forecast.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Yuriy on 18.01.2018.
 */

public class NonSwipeableViewPager extends ViewPager {
    private boolean lock;
    public void lock(){
        lock=true;
    }
    public void unlock(){
        lock=false;
    }
    public NonSwipeableViewPager(Context context) {
        super(context);
    }

    public NonSwipeableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(lock)
            return false;else{
            return super.onTouchEvent(event);
        }
    }
}
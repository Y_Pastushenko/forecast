package com.aeolus.forecast.view;


import android.animation.LayoutTransition;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aeolus.forecast.R;
import com.aeolus.forecast.presenter.IPresenter;
import com.aeolus.forecast.view.vo.DailyWeatherVO;
import com.transitionseverywhere.Rotate;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;

import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 15.01.2018.
 */

public class WeatherFragment extends Fragment {

    public class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new DailyWeatherFragment();
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @BindView(R.id.fragment_weather_view_pager)
    NonSwipeableViewPager viewPager;

    @BindView(R.id.fragment_weather_toolbar)
    Toolbar toolbar;

    @BindView(R.id.fragment_weather_toolbar_title_tv)
    TextView toolbarTitle;

    @BindView(R.id.fragment_weather_toolbar_curtain)
    View toolbarCurtain;

    @BindView(R.id.fragment_weather_toolbar_update_icon)
    ImageView updateIcon;

    @BindView(R.id.fragment_weather_toolbar_change_location_icon)
    ImageView changeLocationIcon;

    @BindView(R.id.fragment_weather_toolbar_layout)
    RelativeLayout toolbarLayout;

    private PagerAdapter pagerAdapter;

    LayoutTransition.TransitionListener transitionListener;

    enum Pages {TODAY, TOMORROW, DAY_AFTER_TOMORROW}

    private static IPresenter presenter;

    private static int currentPage;

    private static DailyWeatherVO currentWeather;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        ButterKnife.bind(this, view);
        pagerAdapter = new PagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                getFragmentAtPosition(currentPage).onPageChanged();
                currentPage = position;
                presenter.onWeatherDaySelected(Pages.values()[position].name());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        viewPager.setOffscreenPageLimit(0);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        updateIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Transition rotate = new Rotate();
                rotate.setDuration(500);
                rotate.addListener(new Transition.TransitionListener() {
                    @Override
                    public void onTransitionStart(Transition transition) {

                    }

                    @Override
                    public void onTransitionEnd(Transition transition) {
                        updateIcon.setRotation(0);
                    }

                    @Override
                    public void onTransitionCancel(Transition transition) {
                        updateIcon.setRotation(0);
                    }

                    @Override
                    public void onTransitionPause(Transition transition) {
                        updateIcon.setRotation(0);
                    }

                    @Override
                    public void onTransitionResume(Transition transition) {

                    }
                });
                TransitionManager.beginDelayedTransition(toolbar, rotate);
                updateIcon.setRotation(360);
                presenter.onUpdatePressed();
            }
        });
        changeLocationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onChangeLocationPressed();
            }
        });
        toolbarCurtain.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (currentWeather != null)
            updateToolbarColor(currentWeather);
    }

    private DailyWeatherFragment getCurrentFragment() {
        return (DailyWeatherFragment) pagerAdapter.instantiateItem(viewPager, viewPager.getCurrentItem());
    }

    private DailyWeatherFragment getFragmentAtPosition(int position) {
        return (DailyWeatherFragment) pagerAdapter.instantiateItem(viewPager, position);
    }

    public void showWeather(DailyWeatherVO weather) {
        getCurrentFragment().showWeather(weather);
        if (weather != WeatherFragment.currentWeather)
            recolorToolbar();
        WeatherFragment.currentWeather = weather;
    }

    public void setPresenter(IPresenter presenter) {
        WeatherFragment.presenter = presenter;
    }

    void updateToolbarColor(DailyWeatherVO weather) {

        toolbarTitle.setText(new DateTime(weather.getDateTime()).toLocalDate().toString());
        switch (weather.getIcon()) {
            case FAIR:
                toolbarTitle.setTextColor(getResources().getColor(R.color.colorAccentSun));
                updateIcon.setImageResource(R.drawable.ic_action_update_sun);
                changeLocationIcon.setImageResource(R.drawable.ic_action_change_location_sun);
                break;
            case PRECIPITATION:
                toolbarTitle.setTextColor(getResources().getColor(R.color.colorAccentRain));
                updateIcon.setImageResource(R.drawable.ic_action_update_rain);
                changeLocationIcon.setImageResource(R.drawable.ic_action_change_location_rain);
                break;
            case CLOUDY:
                toolbarTitle.setTextColor(getResources().getColor(R.color.colorAccentRain));
                updateIcon.setImageResource(R.drawable.ic_action_update_rain);
                changeLocationIcon.setImageResource(R.drawable.ic_action_change_location_rain);
                break;
            default:
                toolbarTitle.setTextColor(getResources().getColor(R.color.colorWhite));
                updateIcon.setImageResource(R.drawable.ic_action_update_white);
                changeLocationIcon.setImageResource(R.drawable.ic_action_change_location_white);
                break;

        }
    }

    private void recolorToolbar() {
        transitionListener = new LayoutTransition.TransitionListener() {
            @Override
            public void startTransition(LayoutTransition layoutTransition, ViewGroup viewGroup, View view, int i) {

            }

            @Override
            public void endTransition(LayoutTransition layoutTransition, ViewGroup viewGroup, View view, int i) {
                if (toolbarCurtain.getVisibility() == View.VISIBLE) {
                    updateToolbarColor(currentWeather);
                    toolbarCurtain.setVisibility(View.INVISIBLE);
                } else {
                    toolbarLayout.getLayoutTransition().removeTransitionListener(transitionListener);
                }
            }
        };
        toolbarLayout.getLayoutTransition().addTransitionListener(transitionListener);
        toolbarCurtain.setVisibility(View.VISIBLE);
    }

    public void lockScreen(boolean lock) {
        if (lock) {
            updateIcon.setOnClickListener(null);
            changeLocationIcon.setOnClickListener(null);
            viewPager.lock();
        }
        else {
            updateIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Transition rotate = new Rotate();
                    rotate.setDuration(500);
                    rotate.addListener(new Transition.TransitionListener() {
                        @Override
                        public void onTransitionStart(Transition transition) {

                        }

                        @Override
                        public void onTransitionEnd(Transition transition) {
                            updateIcon.setRotation(0);
                        }

                        @Override
                        public void onTransitionCancel(Transition transition) {
                            updateIcon.setRotation(0);
                        }

                        @Override
                        public void onTransitionPause(Transition transition) {
                            updateIcon.setRotation(0);
                        }

                        @Override
                        public void onTransitionResume(Transition transition) {

                        }
                    });
                    TransitionManager.beginDelayedTransition(toolbar, rotate);
                    updateIcon.setRotation(360);
                    presenter.onUpdatePressed();
                }
            });
            changeLocationIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.onChangeLocationPressed();
                }
            });
            viewPager.unlock();
        }
    }
}

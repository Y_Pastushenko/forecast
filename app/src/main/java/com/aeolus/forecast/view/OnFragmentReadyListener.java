package com.aeolus.forecast.view;

/**
 * Created by Yuriy on 13.01.2018.
 */

public interface OnFragmentReadyListener {
    void onNestedFragmentReady();
}

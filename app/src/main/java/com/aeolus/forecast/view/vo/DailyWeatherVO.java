package com.aeolus.forecast.view.vo;

import org.joda.time.DateTime;

/**
 * Created by Yuriy on 13.01.2018.
 */

public class DailyWeatherVO {
    public enum Weather {FAIR, CLOUDY, PRECIPITATION}

    private String placeName;
    private long dateTime;
    private double latitude;
    private double longitude;
    private double temperature;
    private double precipitationProbability;
    private double humidity;
    private Weather icon;

    public DailyWeatherVO(String placeName, long dateTime, double latitude, double longitude, double temperature, double precipitationProbability, double humidity, Weather icon) {
        this.placeName = placeName;
        this.dateTime = dateTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.temperature = temperature;
        this.precipitationProbability = precipitationProbability;
        this.humidity = humidity;
        this.icon = icon;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getPrecipitationProbability() {
        return precipitationProbability;
    }

    public void setPrecipitationProbability(double precipitationProbability) {
        this.precipitationProbability = precipitationProbability;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public Weather getIcon() {
        return icon;
    }

    public void setIcon(Weather icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "Name: " + placeName + "\n"
                + "Latitude: " + latitude + "\n"
                + "Longitude: " + longitude + "\n"
                + "Temperature: " + temperature + "\n"
                + "Precipitation probability: " + precipitationProbability + "\n"
                + "Humidity: " + humidity+ "\n"
                + "Icon: " + icon+ "\n"
                + "Date: " + new DateTime(dateTime).toLocalDate();
    }
}

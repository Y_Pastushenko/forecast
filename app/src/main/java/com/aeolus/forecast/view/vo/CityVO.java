package com.aeolus.forecast.view.vo;

/**
 * Created by Yuriy on 15.01.2018.
 */

public class CityVO {
    public enum Weather {FAIR, CLOUDY, PRECIPITATION}

    private String cityName;
    private Weather currentWeather;
    private double temperature;

    public CityVO(String cityName, Weather currentWeather, double temperature) {
        this.cityName = cityName;
        this.currentWeather = currentWeather;
        this.temperature = temperature;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Weather getCurrentWeather() {
        return currentWeather;
    }

    public void setCurrentWeather(Weather currentWeather) {
        this.currentWeather = currentWeather;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof CityVO)) return false;
        CityVO another = (CityVO) other;
        return this.cityName.equals(another.cityName) &&
                (this.currentWeather.name()).equals(another.currentWeather.name()) &&
                this.temperature == another.temperature;
    }
}


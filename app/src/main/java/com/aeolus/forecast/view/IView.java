package com.aeolus.forecast.view;

import android.content.Context;

import com.aeolus.forecast.view.vo.CityVO;
import com.aeolus.forecast.view.vo.DailyWeatherVO;

/**
 * Created by Yuriy on 13.01.2018.
 */

public interface IView {
    void showToast(String message);
    void showExitNotice();
    void showWeather(DailyWeatherVO weather);
    void openWeatherPage();
    void openLocationPage();
    void appendCity(CityVO city);
    void closeApp();
    Context getContext();
    void updateModOn();
    void updateModOff();
}

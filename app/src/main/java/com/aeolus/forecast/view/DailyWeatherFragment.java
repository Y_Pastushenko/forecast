package com.aeolus.forecast.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aeolus.forecast.R;
import com.aeolus.forecast.view.vo.DailyWeatherVO;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Scene;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 13.01.2018.
 */

public class DailyWeatherFragment extends Fragment {

    private class AnimateInTask extends TimerTask {
        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    animateIn();
                }
            });
        }
    }

    private OnFragmentReadyListener listener;
    private DailyWeatherVO dailyWeather;
    private Scene scene1;
    private Scene scene2;
    private View fragmentView;
    private static final int FADE_DURATION = 750;
    private static final int CHANGE_BOUND_DURATION = 500;
    private static final int ANIMATION_IN_DELAY = 200;

    @BindView(R.id.fragment_daily_weather_container)
    RelativeLayout sceneRoot;

    @BindView(R.id.fragment_daily_weather_card_view)
    CardView cardView;

    @BindView(R.id.fragment_daily_weather_name_tv)
    TextView nameTV;

    @BindView(R.id.fragment_daily_weather_temperature_tv)
    TextView temperatureTV;

    @BindView(R.id.fragment_daily_weather_precipitation_probability_tv)
    TextView precipitationProbabilityTV;

    @BindView(R.id.fragment_daily_weather_humidity_tv)
    TextView humidityTV;

    @BindView(R.id.fragment_daily_weather_weather_icon)
    ImageView weather_icon;

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.listener = (OnFragmentReadyListener) context;
        } catch (final ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnCompleteListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_daily_weather, container, false);
        ButterKnife.bind(this, view);
        scene1 = Scene.getSceneForLayout(sceneRoot, R.layout.daily_weather_scene1, getContext());
        scene2 = Scene.getSceneForLayout(sceneRoot, R.layout.daily_weather_scene2, getContext());
        fragmentView = view;
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.onNestedFragmentReady();

    }

    public void showWeather(DailyWeatherVO weather) {
        dailyWeather = weather;
        if (nameTV == null)
            return;
        nameTV.setText(weather.getPlaceName());
        temperatureTV.setText(String.format(getString(R.string.fragment_daily_weather_temperature_tv_text), weather.getTemperature()));
        precipitationProbabilityTV.setText(String.format(getString(R.string.fragment_daily_weather_precipitation_probability_tv_text), weather.getPrecipitationProbability() * 100));
        humidityTV.setText(String.format(getString(R.string.fragment_daily_weather_humidity_tv_text), weather.getHumidity() * 100));
        switch (weather.getIcon()) {
            case FAIR:
                weather_icon.setImageResource(R.drawable.ic_weather_sun);
                break;
            case CLOUDY:
                weather_icon.setImageResource(R.drawable.ic_weather_cloud);
                break;
            case PRECIPITATION:
                weather_icon.setImageResource(R.drawable.ic_weather_rain);
                break;
        }
        new Timer().schedule(new AnimateInTask(), ANIMATION_IN_DELAY);
    }

    public void onPageChanged() {
        animateOut();
    }

    void animateIn() {
        TransitionSet set = new TransitionSet();
        Transition fade = new Fade();
        fade.addTarget(R.id.fragment_daily_weather_weather_icon);
        fade.addTarget(R.id.fragment_daily_weather_card_view);
        fade.setDuration(FADE_DURATION);
        Transition changeBounds = new ChangeBounds();
        changeBounds.setDuration(CHANGE_BOUND_DURATION);
        set.setOrdering(TransitionSet.ORDERING_SEQUENTIAL);
        fade.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {

            }

            @Override
            public void onTransitionEnd(Transition transition) {
                recolor();
            }

            @Override
            public void onTransitionCancel(Transition transition) {

            }

            @Override
            public void onTransitionPause(Transition transition) {

            }

            @Override
            public void onTransitionResume(Transition transition) {

            }
        });
        set.addTransition(fade);
        set.addTransition(changeBounds);
        TransitionManager.go(scene2, set);
        sceneRoot = fragmentView.findViewById(R.id.fragment_daily_weather_container);
        cardView = fragmentView.findViewById(R.id.fragment_daily_weather_card_view);
        sceneRoot = fragmentView.findViewById(R.id.fragment_daily_weather_container);
        nameTV = fragmentView.findViewById(R.id.fragment_daily_weather_name_tv);
        temperatureTV = fragmentView.findViewById(R.id.fragment_daily_weather_temperature_tv);
        precipitationProbabilityTV = fragmentView.findViewById(R.id.fragment_daily_weather_precipitation_probability_tv);
        humidityTV = fragmentView.findViewById(R.id.fragment_daily_weather_humidity_tv);
        weather_icon = fragmentView.findViewById(R.id.fragment_daily_weather_weather_icon);
        nameTV.setText(dailyWeather.getPlaceName());
        temperatureTV.setText(String.format(getString(R.string.fragment_daily_weather_temperature_tv_text), dailyWeather.getTemperature()));
        precipitationProbabilityTV.setText(String.format(getString(R.string.fragment_daily_weather_precipitation_probability_tv_text), dailyWeather.getPrecipitationProbability() * 100));
        humidityTV.setText(String.format(getString(R.string.fragment_daily_weather_humidity_tv_text), dailyWeather.getHumidity() * 100));
        switch (dailyWeather.getIcon()) {
            case FAIR:
                weather_icon.setImageResource(R.drawable.ic_weather_sun);
                break;
            case CLOUDY:
                weather_icon.setImageResource(R.drawable.ic_weather_cloud);
                break;
            case PRECIPITATION:
                weather_icon.setImageResource(R.drawable.ic_weather_rain);
                break;
        }
    }

    void animateOut() {
        TransitionSet set = new TransitionSet();
        Transition fade = new Fade();
        fade.addTarget(R.id.fragment_daily_weather_weather_icon);
        fade.addTarget(R.id.fragment_daily_weather_card_view);
        fade.setDuration(FADE_DURATION);
        Transition changeBounds = new ChangeBounds();
        changeBounds.setDuration(CHANGE_BOUND_DURATION);
        set.setOrdering(TransitionSet.ORDERING_SEQUENTIAL);
        set.addTransition(fade);
        set.addTransition(changeBounds);
        try {
            TransitionManager.go(scene1, set);
        } catch (NullPointerException e) {

        }
    }

    void recolor() {
        nameTV.setTextColor(getResources().getColor(R.color.colorWhite));
        temperatureTV.setTextColor(getResources().getColor(R.color.colorWhite));
        precipitationProbabilityTV.setTextColor(getResources().getColor(R.color.colorWhite));
        humidityTV.setTextColor(getResources().getColor(R.color.colorWhite));
    }
}

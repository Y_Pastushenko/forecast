package com.aeolus.forecast.presenter;

import android.content.Context;

import com.aeolus.forecast.model.IDataManager;
import com.aeolus.forecast.model.dto.DailyWeatherDTO;
import com.aeolus.forecast.view.IView;
import com.aeolus.forecast.view.vo.CityVO;
import com.aeolus.forecast.view.vo.DailyWeatherVO;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Yuriy on 13.01.2018.
 */

public class Presenter implements IPresenter {

    private static final Presenter instance = new Presenter();

    private enum Screen {Weather, Location}

    private Screen actualScreen;

    private enum WeatherDay {TODAY, TOMORROW, DAY_AFTER_TOMORROW}

    private WeatherDay actualWeatherDay;

    class SetBackPressedCountToZeroTask extends TimerTask {
        @Override
        public void run() {
            Presenter.backPressCount = 0;
        }
    }

    private static final String OUT_OF_FREE_API_CALLS_MESSAGE = "Out of free api calls";

    private IView view;
    private IDataManager dataManager;
    private List<DailyWeatherVO> weatherForCurrentCity;
    private List<CityVO> cities;
    private String currentCityName;
    private static volatile int backPressCount = 0;
    private static final int BACK_PRESSURES_TO_EXIT = 3;
    private static final int SET_BACK_PRESSED_COUNT_TO_ZERO_DELAY = 3000;

    private Presenter() {
    }

    public static Presenter getInstance() {
        return instance;
    }

    public void setDataManager(IDataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void setView(IView view) {
        this.view = view;
    }

    @Override
    public void onInit() {
        dataManager.getLastCity().subscribe(s -> {
            currentCityName = s;
            switchPageTo(Screen.Weather);
            cities = new ArrayList<>();
        });
    }

    @Override
    public void onViewCreated() {
        update();
    }

    @Override
    public void onWeatherDaySelected(String day) {
        actualWeatherDay = WeatherDay.valueOf(day);
        if (weatherForCurrentCity != null)
            showActualWeather();
    }

    @Override
    public void onChangeLocationPressed() {
        view.updateModOn();
        switchPageTo(Screen.Location);
    }

    @Override
    public void onCityChosen(String cityName) {
        if (!currentCityName.equals(cityName)) {
            currentCityName = cityName;
            dataManager.saveLastCity(cityName);
            weatherForCurrentCity.clear();
        }
        switchPageTo(Screen.Weather);
    }

    @Override
    public void onUpdatePressed() {
        cities.clear();
        weatherForCurrentCity.clear();
        update();
    }

    @Override
    public void onOutOfFreeApiCalls() {
        view.showToast(OUT_OF_FREE_API_CALLS_MESSAGE);
    }

    @Override
    public void onBackPressed() {
        backPressCount++;
        SetBackPressedCountToZeroTask task = new SetBackPressedCountToZeroTask();
        new Timer().schedule(task, SET_BACK_PRESSED_COUNT_TO_ZERO_DELAY);
        if (backPressCount == 1) {
            switch (actualScreen) {
                case Location:
                    switchPageTo(Screen.Weather);
            }
            return;
        }
        if (backPressCount == BACK_PRESSURES_TO_EXIT - 1) {
            view.showExitNotice();
            return;
        }
        if (backPressCount == BACK_PRESSURES_TO_EXIT)
            view.closeApp();
    }

    @Override
    public Context getContext() {
        return view.getContext();
    }

    private void update() {
        view.updateModOn();
        switch (actualScreen) {
            case Weather:
                if (weatherForCurrentCity == null || weatherForCurrentCity.isEmpty()) {
                    dataManager.getWeatherByCityName(currentCityName, 3).subscribe(l -> {
                        weatherForCurrentCity = Stream.of(l).map(dto -> weatherDtoToVO(dto)).toList();
                        showActualWeather();
                        view.updateModOff();
                    });
                } else {
                    showActualWeather();
                    view.updateModOff();
                }
                break;
            case Location:
                dataManager.getCitiesNumber().subscribe(n -> {
                    if (!n.equals(cities.size()))
                        dataManager.getCities().subscribe(list -> {
                            for (String s : list) {
                                dataManager.getWeatherByCityName(s, 1).subscribe(w -> {
                                    view.updateModOff();
                                    CityVO city = weatherDtoToCityVO(w.get(0));
                                    view.appendCity(city);
                                    if (!cities.contains(city))
                                        cities.add(city);
                                });
                            }
                        });
                    else {
                        view.updateModOff();
                        for (CityVO city : cities)
                            view.appendCity(city);
                    }
                });
                break;
        }
    }

    private void switchPageTo(Screen screen) {
        backPressCount = 0;
        switch (screen) {
            case Weather:
                actualScreen = Screen.Weather;
                actualWeatherDay = WeatherDay.TODAY;
                view.openWeatherPage();
                break;
            case Location:
                actualScreen = Screen.Location;
                view.openLocationPage();
                break;
        }
    }

    private void showActualWeather() {
        switch (actualWeatherDay) {
            case TODAY:
                view.showWeather(weatherForCurrentCity.get(0));
                break;
            case TOMORROW:
                view.showWeather(weatherForCurrentCity.get(1));
                break;
            case DAY_AFTER_TOMORROW:
                view.showWeather(weatherForCurrentCity.get(2));
                break;
        }
    }

    private DailyWeatherVO weatherDtoToVO(DailyWeatherDTO dto) {
        return new DailyWeatherVO(dto.getPlaceName(),
                dto.getDateTime(),
                dto.getLatitude(),
                dto.getLongitude(),
                dto.getTemperature(),
                dto.getPrecipitationProbability(),
                dto.getHumidity(),
                DailyWeatherVO.Weather.valueOf(dto.getIcon().name()));
    }

    private DailyWeatherDTO weatherVoToDTO(DailyWeatherVO vo) {
        return new DailyWeatherDTO(vo.getPlaceName(),
                vo.getDateTime(),
                vo.getLatitude(),
                vo.getLongitude(),
                vo.getTemperature(),
                vo.getPrecipitationProbability(),
                vo.getHumidity(),
                DailyWeatherDTO.Weather.valueOf(vo.getIcon().name()));
    }

    private CityVO weatherDtoToCityVO(DailyWeatherDTO dto) {
        return new CityVO(dto.getPlaceName(), CityVO.Weather.valueOf(dto.getIcon().name()), dto.getTemperature());
    }

}

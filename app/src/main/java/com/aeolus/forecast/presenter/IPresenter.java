package com.aeolus.forecast.presenter;

import android.content.Context;

import com.aeolus.forecast.view.IView;

/**
 * Created by Yuriy on 13.01.2018.
 */

public interface IPresenter {
    void onViewCreated();
    void onWeatherDaySelected(String page);
    void onChangeLocationPressed();
    void setView(IView weatherPage);
    void onInit();
    void onCityChosen(String cityName);
    void onUpdatePressed();
    void onOutOfFreeApiCalls();
    void onBackPressed();
    Context getContext();
}

package com.aeolus.forecast.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.aeolus.forecast.R;
import com.aeolus.forecast.model.dto.DailyWeatherDTO;
import com.aeolus.forecast.model.dto.LatLng;
import com.aeolus.forecast.model.net.NetManager;
import com.aeolus.forecast.presenter.IPresenter;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Yuriy on 13.01.2018.
 */

public class DataManager implements IDataManager {

    class NoSuchCityException extends Exception {
    }

    private static final DataManager instance = new DataManager();
    private static IPresenter presenter;

    private static HashMap<String, LatLng> cities = new HashMap<>();
    private static NetManager netManager;
    private static final String LAST_CITY_NAME_KEY = "last_city_name_key";
    private static final String DEFAULT_CITY = "Киев";

    private DataManager() {
    }

    private static void fillInCities(HashMap<String, LatLng> cities) {
        cities.put("Киев", new LatLng(50.45466, 30.5238));
        cities.put("Амстердам", new LatLng(52.367, 4.883));
        cities.put("Афины", new LatLng(37.967, 23.717));
        cities.put("Берлин", new LatLng(52.45, 13.3));
        cities.put("Лиссабон", new LatLng(38.700, -9.15));
        cities.put("Москва", new LatLng(55.75, 37.833));
        cities.put("Нью-Йорк", new LatLng(40.750, -73.967));
        cities.put("Рим", new LatLng(41.8, 12.233));
        cities.put("Осло", new LatLng(59.917, 10.733));
        cities.put("Мельбурн", new LatLng(-37.833, 145));
        cities.put("Мадрид", new LatLng(40.4, -3.683));
        cities.put("Лондон", new LatLng(51.517, -0.1));
        cities.put("Тест-Сити", new LatLng(51.517, -0.1));
    }

    public static DataManager getInstance() {
        if (cities.isEmpty())
            fillInCities(cities);
        if (netManager == null)
            netManager = new NetManager(presenter);
        return instance;
    }

    public static void setPresenter(IPresenter presenter) {
        DataManager.presenter = presenter;
    }

    @Override
    public Observable<Integer> getCitiesNumber() {
        return Observable.just(cities.size());
    }

    @Override
    public Observable<List<String>> getCities() {
        return Observable.just(Stream.of(cities.keySet()).toList());
    }

    @Override
    public Observable<List<DailyWeatherDTO>> getWeatherByCityName(String cityName, int limit) {
        if (cityName.equals("Тест-Сити")) {
            List<DailyWeatherDTO> l = new ArrayList<>();
            l.add(new DailyWeatherDTO("Киев", 1516217204177L, 0, 0, 21, 0.56, 0.78, DailyWeatherDTO.Weather.FAIR));
            l.add(new DailyWeatherDTO("Киев", 1516226400000L, 0, 0, -8, 0, 0.63, DailyWeatherDTO.Weather.CLOUDY));
            l.add(new DailyWeatherDTO("Киев", 1516312800000L, 0, 0, 5, 0, 0.68, DailyWeatherDTO.Weather.PRECIPITATION));
            return Observable.just(l).delay(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread());
        }
        if (cities.containsKey(cityName))
            return netManager.getForecastInPlace(cities.get(cityName).getLatitude(), cities.get(cityName).getLongitude(), limit).map(p -> {
                for (DailyWeatherDTO w : p) w.setPlaceName(cityName);
                return p;
            });
        return Observable.error(new NoSuchCityException());
    }

    @Override
    public Observable<List<DailyWeatherDTO>> getWeatherByCoordinates(double latitude, double longitude, int limit) {
        return netManager.getForecastInPlace(latitude, longitude, limit);
    }

    @Override
    public void saveLastCity(String cityName) {
        SharedPreferences sharedPref = presenter.getContext().getSharedPreferences(presenter.getContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(LAST_CITY_NAME_KEY, cityName);
        editor.apply();
    }

    @Override
    public Observable<String> getLastCity() {
        SharedPreferences sharedPref = presenter.getContext().getSharedPreferences(presenter.getContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return Observable.just(sharedPref.getString(LAST_CITY_NAME_KEY, DEFAULT_CITY));
    }
}


package com.aeolus.forecast.model.net.retrofitModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherForWeek {

    @SerializedName("data")
    @Expose
    private List<CompleteDailyWeather> data = null;

    public List<CompleteDailyWeather> getData() {
        return data;
    }

    public void setData(List<CompleteDailyWeather> data) {
        this.data = data;
    }

}

package com.aeolus.forecast.model.net;

import com.aeolus.forecast.model.dto.DailyWeatherDTO;
import com.aeolus.forecast.model.net.retrofitModel.CompleteDailyWeather;
import com.aeolus.forecast.model.net.retrofitModel.ForecastResponse;
import com.aeolus.forecast.presenter.IPresenter;
import com.annimon.stream.Stream;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yuriy on 29.09.2017.
 */

public class NetManager {
    private static IPresenter presenter;
    private static final String apiKey = "aecfad31a08670428bbd65b7bb13e693";
    private OpenWeatherMapApi openWeatherMapApi;
    private Retrofit retrofit;

    public NetManager(IPresenter presenter) {
        NetManager.presenter = presenter;
    }

    private OpenWeatherMapApi getOpenWeatherMapApi() {
        if (openWeatherMapApi == null || retrofit == null) {
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl("https://api.darksky.net/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            openWeatherMapApi = retrofit.create(OpenWeatherMapApi.class);
        }
        return openWeatherMapApi;
    }

    public Observable<List<DailyWeatherDTO>> getForecastInPlace(double latitude, double longitude, int limit) {
////        //ToDo comment out calls when debugging
//        List<DailyWeatherDTO> l = new ArrayList<>();
//        l.add(new DailyWeatherDTO("Киев",1516217204177L,0,0,21,0.56,0.78, DailyWeatherDTO.Weather.FAIR));
//        l.add(new DailyWeatherDTO("Киев",1516226400000L,0,0,-8,0,0.63, DailyWeatherDTO.Weather.CLOUDY));
//        l.add(new DailyWeatherDTO("Киев",1516312800000L,0,0,5,0,0.68, DailyWeatherDTO.Weather.PRECIPITATION));
//        return Observable.just(l).delay(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread());

        return Observable.create(emitter -> {
            OpenWeatherMapApi api = getOpenWeatherMapApi();
            api.getCityForecast(constructForecastRequest(apiKey, latitude, longitude)).enqueue(new Callback<ForecastResponse>() {
                @Override
                public void onResponse(Call<ForecastResponse> call, Response<ForecastResponse> response) {
                    if (response.isSuccessful()) {
                        List<CompleteDailyWeather> data = response.body().getWeatherForWeek().getData();

                        List<DailyWeatherDTO> weatherList = Stream.of(data).map(d ->
                                new DailyWeatherDTO("",
                                        Long.valueOf("" + d.getTime())*1000,
                                        latitude,
                                        longitude,
                                        d.getApparentTemperatureHigh(),
                                        d.getPrecipProbability(),
                                        d.getHumidity(),
                                        reduceWeatherTypes(d.getIcon())))
                                .toList();
                        emitter.onNext(weatherList.subList(0, limit));
                        emitter.onComplete();
                    } else {
                        if (response.code() == 403)
                            presenter.onOutOfFreeApiCalls();
                        emitter.onComplete();
                    }
                }

                @Override
                public void onFailure(Call<ForecastResponse> call, Throwable t) {
                    emitter.onComplete();
                    if (t instanceof UnknownHostException)
                        //dataManager.onNetworkLost();
                        return;
                }
            });
        });
    }

    private String constructForecastRequest(String apiKey, double latitude, double longitude) {
        return "https://api.darksky.net/forecast/" + apiKey + "/" + latitude + "," + longitude + "?units=si";
    }

    private DailyWeatherDTO.Weather reduceWeatherTypes(String icon) {
        switch (icon) {
            case "clear-day":
                return DailyWeatherDTO.Weather.FAIR;
            case "clear-night":
                return DailyWeatherDTO.Weather.FAIR;

            case "fog":
                return DailyWeatherDTO.Weather.CLOUDY;
            case "cloudy":
                return DailyWeatherDTO.Weather.CLOUDY;
            case "partly-cloudy-night":
                return DailyWeatherDTO.Weather.CLOUDY;
            case "partly-cloudy-day":
                return DailyWeatherDTO.Weather.CLOUDY;

            case "rain":
                return DailyWeatherDTO.Weather.PRECIPITATION;
            case "snow":
                return DailyWeatherDTO.Weather.PRECIPITATION;
            case "hail":
                return DailyWeatherDTO.Weather.PRECIPITATION;

            default:
                return DailyWeatherDTO.Weather.CLOUDY;
        }
    }

}

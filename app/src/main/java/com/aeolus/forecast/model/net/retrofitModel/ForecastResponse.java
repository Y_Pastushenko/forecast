
package com.aeolus.forecast.model.net.retrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForecastResponse {

    @SerializedName("daily")
    @Expose
    private WeatherForWeek weatherForWeek;

    public WeatherForWeek getWeatherForWeek() {
        return weatherForWeek;
    }

    public void setWeatherForWeek(WeatherForWeek weatherForWeek) {
        this.weatherForWeek = weatherForWeek;
    }

}

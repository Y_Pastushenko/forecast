package com.aeolus.forecast.model;

import com.aeolus.forecast.model.dto.DailyWeatherDTO;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 13.01.2018.
 */

public interface IDataManager {

    Observable<List<String>> getCities();
    Observable<List<DailyWeatherDTO>> getWeatherByCityName(String cityName, int limit);
    Observable<List<DailyWeatherDTO>> getWeatherByCoordinates(double latitude, double longitude, int limit);
    Observable<Integer> getCitiesNumber();
    void saveLastCity(String cityName);
    Observable<String> getLastCity();
}

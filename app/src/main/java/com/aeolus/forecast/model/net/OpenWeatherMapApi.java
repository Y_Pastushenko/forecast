package com.aeolus.forecast.model.net;

import com.aeolus.forecast.model.net.retrofitModel.ForecastResponse;

import retrofit2.Call;
import retrofit2.http.GET;

import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Yuriy on 29.09.2017.
 */

interface OpenWeatherMapApi {

    @GET
    Call<ForecastResponse> getCityForecast(@Url String url);

}
